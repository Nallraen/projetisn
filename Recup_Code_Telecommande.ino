#include "IRremote.h"
int IR_PIN = 17;

IRrecv receiver(IR_PIN);

decode_results output;

void setup() {
  Serial.begin(9600);
  receiver.enableIRIn(); // Activation du capteur IR
}

void loop() {
  if (receiver.decode(&output)) { // On check si une touche est appuyée
    unsigned int value = output.value; // On assigne une valeur INT à la touche
    Serial.println(value); // On print la valeur INT de la touche dans la console
    receiver.resume(); // On continue...
  }
}
