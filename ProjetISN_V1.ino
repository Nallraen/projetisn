// Codes des Boutons :
//  32895 -> VOL +
//  36975 -> VOL -
//  24735 -> Droite
//  8415 -> Gauche
//  255 -> Stop

#include "IRremote.h"


#define vol_plus 32895 // Bouton pour avancer
#define vol_moins 36975 // Bouton pour reculer
#define droite 24735 // Bouton pour aller à droite
#define gauche 8415 // Bouton pour aller à gauche
#define arret 255 // Bouton pour arrêter la voiture
#define EN1 6 // Entrée d'activation/désactivation du moteur droit
#define IN1 7 // Entrée de controle du moteur droit
#define EN2 5 // Entrée d'activation/désactivation du moteur gauche  
#define IN2 12 // Entrée de controle du moteur gauche

int IR_PIN = 17; // Entrée du capteur IR

// Fonctions proprent au traitement de l'IR
IRrecv irrecv(IR_PIN); 
decode_results results;


void setup()
{
  pinMode(5,OUTPUT); // Initialisation des moteurs
  pinMode(6,OUTPUT);
  pinMode(7,OUTPUT);
  pinMode(12,OUTPUT);
  Serial.begin(9600); // Création d'un canal pour la réception des codes des boutons (Experimental, inutile pour le fonctionnement final)
  irrecv.enableIRIn(); // Activation de l'IR

  digitalWrite(5,LOW); // On coupe tout les moteurs
  digitalWrite(6,LOW);
  digitalWrite(7,LOW);
  digitalWrite(12,LOW);
  
}

void loop()
{
  if(irrecv.decode(&results)){ // On regarde si l'IR reçoit qlq chose
    unsigned int value = results.value; // On traduit la valeur reçu en un code avec une valeur INT
    switch(value) // On check si notre programme gère la touche appuyé (avec un switch c'est plus classe que les if(){}.......
    {
      case vol_plus: 
        digitalWrite(5,HIGH);
        digitalWrite(6,HIGH);
        digitalWrite(7,HIGH);
        digitalWrite(12,HIGH);
        break;
      case vol_moins:
        digitalWrite(5, HIGH);
        digitalWrite(6, HIGH);
        digitalWrite(7,LOW);
        digitalWrite(12,LOW);
        break;
      case gauche:
        digitalWrite(5,HIGH);
        digitalWrite(6,HIGH);
        digitalWrite(7,LOW);
        digitalWrite(12,HIGH);
        break;
      case droite:
        digitalWrite(5,HIGH);
        digitalWrite(6,HIGH);
        digitalWrite(7,HIGH);
        digitalWrite(12,LOW);
        break;
      case arret:
        digitalWrite(5,LOW);
        digitalWrite(6,LOW);
        digitalWrite(7,LOW);
        digitalWrite(12,LOW);
        break;
    }
    
    Serial.println(results.value, HEX); // Utiliser pour récupérer les codes des boutons inutiles maintenant pour le final.
    irrecv.resume(); // On dit à l'IR de continuer son job
  }
  delay(100);
}
